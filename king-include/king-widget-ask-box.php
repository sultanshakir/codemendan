<?php

/*
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	More about this license: LICENCE.html
*/

	class qa_ask_box {
		
		function allow_template($template)
		{
			$allow=false;
			
			switch ($template)
			{
				case 'activity':
				case 'categories':
				case 'custom':
				case 'feedback':
				case 'qa':
				case 'questions':
				case 'hot':
				case 'search':
				case 'tag':
				case 'tags':
				case 'unanswered':
					$allow=true;
					break;
			}
			
			return $allow;
		}

		
		function allow_region($region)
		{
			$allow=false;
			
			switch ($region)
			{
				case 'main':
				case 'side':
				case 'full':
					$allow=true;
					break;
			}
			
			return $allow;
		}
	
	
		function output_widget($region, $place, $themeobject, $template, $request, $qa_content)
		{
			if (isset($qa_content['categoryids']))
				$params=array('cat' => end($qa_content['categoryids']));
			else
				$params=null;
?>
<DIV CLASS="askbox">
	<FORM METHOD="POST" ACTION="<?php echo qa_path_html('ask', $params); ?>">
		<TABLE>
			<TR>

					<INPUT CLASS="custom-ask-submit" TYPE="submit" value="" title="video ekle">

<?php
			if ($region=='side') {
?>
			</TR>
			<TR>
<?php			
			}
?>
				
					<INPUT NAME="title" TYPE="text" CLASS="askkutu" placeholder="videonuz için bir başlık girin !">
				
			</TR>
		</TABLE>
		<INPUT TYPE="hidden" NAME="doask1" VALUE="1">
	</FORM>
</DIV>
<?php
		}
	
	}
	

/*
	Omit PHP closing tag to help avoid accidental output
*/